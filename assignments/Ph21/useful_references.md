The following are some useful references to learn the materials and complete the assignments.

# Assignment 21.1

# Assignment 21.2

# Assignment 21.3

Online materials:
1. Edge detection: http://dept.me.umn.edu/courses/me5286/vision/VisionNotes/2017/ME5286-Lecture7-2017-EdgeDetection2.pdf
2. Implementing Canny's Edge detection in Python: https://towardsdatascience.com/canny-edge-detection-step-by-step-in-python-computer-vision-b49c3a2d8123
3. Canny's original paper: https://doi.org/10.1109/TPAMI.1986.4767851

Useful packages:
1. [OpenCV](https://github.com/skvark/opencv-python)
2. [Pillow](https://github.com/python-pillow/Pillow)
3. [scikit-image](https://scikit-image.org/)


# Assignment 21.4b

Online materials:
1. A 5-min introduction on MCMC and Metropolis-Hastings: https://youtu.be/OTO1DygELpY
2. A short tutorial on using `pymc3` to solve a (slightly more complicated) coin-tossing problem: https://medium.com/airy-science/bayesian-inference-with-probabilistic-programming-using-pymc3-a00702ccd9e0
3. An intro to MCMC and using `pymc3`: https://nbviewer.jupyter.org/github/CamDavidsonPilon/Probabilistic-Programming-and-Bayesian-Methods-for-Hackers/blob/master/Chapter3_MCMC/Ch3_IntroMCMC_PyMC3.ipynb

# Assignment 21.5
