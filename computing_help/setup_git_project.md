# How to set up your GitLab project(s)  [ajw, v.20210104.1]

* Go to the main course project site: https://bayard.caltech.edu/ph2x/info and take a look at the contents.
(bayard is a GitLab site maintained by the Caltech Physics computing lab.)    

* If you haven't already signed up, go to https://bayard.caltech.edu/users/sign_in
    * Click on Register, fill in the fields, using your caltech.edu email, and create (and remember!) a password that you will ONLY USE FOR THIS SITE, then click Register.
    * (this GitLab site is not yet set up for caltech single-signon).    

<br/>

* Go to https://bayard.caltech.edu/
    * Click on New Project on the top right corner
    * Give your project a name (eg, Your_Name_ph21). We recommend avoiding the use of spaces; use underscores or dashes instead.
    * Visibility level should be Private.
    * Check "Initialize repository with a README". (recommended since it also automatically creates a default branch for your projects)
    * Then click Create Project. Verify the URL of your project takes the form "https://bayard.caltech.edu/YOUR_USERNAME/YOUR_PROJECTNAME/". Save the URL so that you can get back there easily.
    * On the menu on the left, click on Settings -> General. Under Visibility, verify that it's Private.
    * Click on Settings -> Members. Invite Member, add ajw and your TA (at least "reporter" of the project to see your project in their homepages).

<br/>

* Using the GitLab web user interface (UI): On the menu on the left, click Repository -> Files.
    * From here, you can create new files (with the "+" near the top), upload files, edit files, etc.
    * For example, you can create folders for each assignment, upload your *.py , *.ipynb, *.m, *.tex, *.pdf files.
    * GitLab knows how to display these files nicely.
    * Send the url for your project to your TA, who will be able to see all your files.
    * Git will keep all versions of every file, with a history log.

<br/>

* git and Gitlab can do much, much more. It's main power is in collaborative work to develop large coding projects.
    * There are many tools for developing, reviewing, testing, packaging, releasing, etc.
    * You can learn about this by browsing the many help resources, such as
    * https://bayard.caltech.edu/help
    * https://docs.gitlab.com/ee/
    * https://docs.gitlab.com/ee/gitlab-basics/
    * https://docs.gitlab.com/ee/university/training/topics/getting_started.html
    * https://www.tutorialspoint.com/gitlab/index.htm
    * https://rogerdudler.github.io/git-guide/

<br/>

* You will probably want to "clone" your project onto your own computer 
  * git is pre-installed on mac or linux
  * on windows, you'll need https://git-scm.com/ (tested; works well!) 
    or https://gitforwindows.org/ (if you try this and like it, let us know.)
  * Then, follow the directions in:
    * https://docs.gitlab.com/ee/gitlab-basics
    * https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html - this is an essential step
    * https://docs.gitlab.com/ee/gitlab-basics/create-project.html
    * https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html
    * https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html

<br/>

* You can then use commands like these from a terminal on your computer: 
    * git clone git@bayard.caltech.edu/PATH_TO_YOUR_PROJECT.git # use your ssh key passphrase.
    * and then pull the project contents, make changes, then push your changes back up to the gitlab repository:
    * https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html
    * https://docs.gitlab.com/ee/university/training/topics/getting_started.html
    * https://rogerdudler.github.io/git-guide/

  