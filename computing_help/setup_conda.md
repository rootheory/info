# Setup Anaconda

- The easiest way to set up a python environment in your personal computer is with [anaconda](https://www.anaconda.com/). 
- It even works on Windows! The standard version is free. This is strongly recommended!
- Since `python` is a wonderfully "extensible" language, there are (very) many useful packages (numpy, scipy, matplotlib, astropy, scikit-learn, ...) that you will want to use.
- Anaconda serves as a package manager, with a long list of [3rd-party packages](https://docs.anaconda.com/anaconda/packages/pkg-docs/) that it knows about.
- We will be using python 3. Take care - managing different python environments is [notoriously problematic](https://xkcd.com/1987/)!
- Learn the basics: https://docs.anaconda.com/anaconda/user-guide/getting-started/
- You can launch anaconda navigator, and launch a jupyter notebook from there; or go straight to the jupyter notebook. Start coding!
- For more flexibility, you can set up a conda virtual environment, with python 3 (v3.7).
- Make sure that jupyter is installed in your conda virtual environment.
- In particular, learn how to create and use jupyter notebooks (or, use spyder if you prefer).
- Go ahead: write a simple python program in a jupyter notebook. 
  - Make a plot or two using matplotlib. 
  - Put in a cell or two of explanation, using markdown (.md) syntax.
  - Give your notebook a meaningful name, save on your disk, then upload it to your GitLab project. Display it there.
  - Use the GitLab issue tracker to tell your TA (eg, @jacob ) to look at it. 

# Alternatives to conda
- If you have a Mac, with Xcode installed, you can do it other ways; eg, MacPorts or just "pip install". Newbies will prefer anaconda.
