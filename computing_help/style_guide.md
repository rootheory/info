It's really important for your code and your writeups to be clear and understandable to all, because "science not communicated is science not done".

- Python code should have enough comments to help someone read and understand what the code is doing; but you don't have to comment every single line of code. See, for example, https://realpython.com/python-comments-guide/

- Python variable names should be informative (not "a = 10"). But they don't need to be very very long. See, eg, the style guide: https://www.python.org/dev/peps/pep-0008/ and in particular, https://www.python.org/dev/peps/pep-0008/#function-and-variable-names

- You can submit python code (meaningful_filename.py) and associated plots (in png or pdf, again, with meaningful names); but ...

- Better to use jupyter notebooks, with markdown cells that explain what you are doing. Gitlab displays ipynb files pretty well (for some reason, it does NOT display html files well!).

- Or, use Gitlab's markdown language; eg, create a file like Ph21_A1.md . You can embed plots, formulas, etc. Learning markdown is easy!

- Even better: use latex (eg, with the help of overleaf.com). Write something that looks like a real scientific publication, with title, abstract, sections, figures with embedded plots and captions, tables with captions, citations, etc. Upload a pdf fo your TA to grade. 

- Please, do not name your files something uninformative, like "main.pdf" ; a good filename might be "Fistname_lastname_Ph21_A1_date.pdf" . A good date format is YYYYMMDD; eg, 20200410 .

- In the jupyter notebook, you can add markdown cells to divide the code into different sections.  

- Learn how to write professionally!
