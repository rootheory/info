# Ph 20/21/22: Guidelines and Ground Rules

## Staff, Spring 2021

- Lab Manager: Chris Mach (103-33, x2299, chris{at}alice)
- Prof/Admin: Alan Weinstein, ajw{at}caltech{dot}edu
- Ph 20, 21, 22 TAs: Jacob Shen, xshen{at}caltech{dot}edu
- Ph 21 TA: Liam O'Brien, lobrien{at}caltech{dot}edu

---
## The laboratory

- Physics 20, 21 and 22 - These are three terms (6 units each) of a computational physics lab.
- Ph 20 is offered in fall, winter, spring terms. Nominally, CS 1 is a pre-req.
- Ph 21 is offered in fall, winter and spring. Nominally, Ph 20 is a pre-req for 21.
- Ph 22 is offered in winter and spring. Nominally, Ph 21 is a pre-req for Ph 22.
- The course website (a git repository) with all the assignments and other resources: https://bayard.caltech.edu/ph2x/info/ . This is a GitLab site, with many useful features.
- The purpose of Ph20/21/22 is for you to gain familiarity with the tools and techniques of computational physics, both for simulating and solving simple physical systems. The emphasis is on writing programs to model physical systems, and using various tools to solve physics problems.
- The lab is not about how to program, but rather how to use computers to solve problems in physics. Physics 20 requires completion of or placement out of CS 1.
- You can do all of your work on your own computer, logging in to your own git repo on https://bayard.caltech.edu .
- However, you also have the Caltech Physics linux computing lab cluster at your disposal for more compute-intensive work (maybe useful for Ph 22).
- Chris Mach can set up Linux accounts and directories for all Ph20/21/22 students. For this, he will need (a) full name, (b) e-mail address (where you can be reached), (c) requested login name (whatever you want, not case-sensitive), and (d) initial password (case-sensitive), which you can change with `passwd`. Do not use a password that you use anywhere else! This is just for the physlab computing cluster.
- If you want access to the lab after 5 pm, you can get a key to the building in 105 E. Bridge. The combination for the lock to the lab door can be obtained from the TAs or Chris.

---
## Working remotely (Spring 2021)

- In these extraordinary circumstances, we don't have the benefit of being face to face with the Ph 20/21/22 TAs.
- Instead, we will work "screen-to-screen" using zoom: https://caltech.zoom.us/ . Logon with your access.caltech account.
- Our organizational meeting will be on zoom in the first week (watch your email from Prof Weinstein). 
- You will sort into one of several groups, each led by one of the TAs, which will meet via zoom every week.
- You *are required* to attend their assigned zoom session for at least *one hour every week*. 
- This is your opportunity to meet with your TA and fellow students to discuss the class material and learn how to best approach the assignments;
it will include (a) learning about the physics; (b) learning how to use the computer to solve the problem; (c) preparing your solutions for grading by your TA.
- You will need to keep your computer and internet connection in a healthy state; this goes for all of your classes during remote learning. 
Your TAs can give advice, but can't really help if things go wrong on your end. You can get advice and help at http://learn.caltech.edu/ .
- For example, it may prove useful to chat with your TA and fellow students using http://learn.caltech.edu/tech-tools/google-hangouts .
- It might take some time to work out the best use of these online resources. This is all a new experience for everyone!

---
## Course material

- The class website https://bayard.caltech.edu/ph2x/info includes all of the assignments, and links to various resources.
- Because scientific computing is constantly changing and growing in scope, some of the links are probably already obsolete, 
and there are surely countless other resources that we could link to. 
You will likely find more modern tools and techniques... good! Share with your TAs, who will try to update the material as they go. 
- All students are asked to point out obsolete stuff on the website, and suggest improvements!

---
## Working with your own git repo and your own computing environment

- You will set up your project in the Ph2x GitLab site, and upload your assignment solutions there, for iteration with your TA until your TA approves it.
- You will set up your computing environment for python, mathematica, LaTex, GitLab, etc. 

---
## The assignments

- Each of the courses consists of several assignments (eg, for Ph 20, there are 7).
- To pass the course, you must turn in a passing version of **all of them**.
- Some assignments are shorter and are due the following week; other are longer or harder, and are due two weeks later. See the course website for due dates (which may be subject to change as the term progresses!)
- If your TA is not satisfied with your work, you will get your assignment back and have the opportunity to revise it.
- Your TA is the final judge of what will earn you a passing grade for a given assignment.
- All assignments (in pdf), as well as some useful additional information, are posted on the ph20/21/22 website. On the lab computers, you should run firefox from your Linux account. If you are using your laptop, you're welcome to use your favorite browser.
- Assignments tend to be cumulative (what you learn in one assignment will be needed for future ones), and they will get increasingly harder.
- Some parts of the assignments (generally labeled with stars) are meant to be challenging, even for the best students. You may leave them undone if you think that you have already spent a reasonable amount of time working the rest of the assignment.
- You will maintain a [git repository](https://en.wikipedia.org/wiki/Git) to maintain your code, your assignment writeups, and any other relevant files.
- The TAs will explain what a git repo is, what it's for, how to set one up, and how to use it. There are many online tutorials, such as [this one on edureka](https://www.edureka.co/blog/how-to-use-github/)
- Assignments can be turned in by posting your solutions in your git repo project.
- Generally, you will turn in a brief written description of your work, with explanatory graphs.
- The best way to do this is to post your code as a jupyter notebook (with filetype *.ipynb) or mathematica notebook.
- You should present some or all of your work using LaTeX. If you don't know it, learn it! 
An easy way to get started (and to collaborate) is with [overleaf](https://www.overleaf.com/).
- Learn how to embed your code, and your graphs and plots as figures, into your LaTeX.
- **Do not fall behind!** and keep in mind that the assignments typically get harder and longer as the course progresses.
- Because it will be extremely difficult to catch up with the assignments if you fall significantly behind, late assignments will be accepted **only with prior permission of your TA.** Furthermore, the maximum extension allowed will be **one week** after the due date, except in extreme circumstances such as serious illness, etc. Each student is generally allowed two one-week extensions during the quarter.
- If multiple assignments are outstanding and a student is significantly behind in the course, the student will be advised to drop the course.
- The due dates for the assignments will be posted on the website.

----
## Collaboration Policy 

- You may work collaboratively in discussing the problem and the general approach to the solution of the problem 
including relevant equations and appropriate numerical techniques. 
However, you may not collaborate in the writing of the code for solution of the problem: 
the actual computer program for the solution of the problem must be your own.

---
## The sections

- There will be two or three three-hour formal lab sections each week, at the times determined at the organizational meeting.
- For example, we may agree that these will be on Tuesdays 1-4 pm and Fridays 1-4 pm.
- You must sign up for one of these, and check in with the appropriate TA during that session.
- Please stick to one of these two days, and one of the TAs.
- The assignments often require significant explanation, and the TAs will do that explaining during the formal lab sessions.
- We **require** that you show up for (a minimum of) one hour out of the three, to receive any handouts, read any messages on the boards, hear what your TA has to say, and so on.
- The lab is available at all times, but you are also welcome to use your own computer or other facilities to complete the assignments.
- Even if you do so, you are still required to check in at your section. Failure to attend the weekly lab session may lead to our advising the student to drop the course.

---
## Computers & software

- You can use the physlab computer cluster, either by sitting in front of a workstation and logging in with the username / password 
supplied to you by Chris Mach, or by logging in remotely with `ssh <your-username>@tweedledee.caltech.edu` . 
But many of you will prefer to use your own computer, especially in this time of covid.
- The easiest way to set up a python environment in your personal computer is with [anaconda](https://www.anaconda.com/). 
It even works on Windows! The standard version is free. This is strongly recommended!
- If you have a Mac, with Xcode installed, you can do it other ways; eg, MacPorts or just "pip install". Newbies will prefer anaconda.
- Since `python` is a wonderfully "extensible" language, there are (very) many useful packages (numpy, scipy, matplotlib, astropy, scikit-learn, ...) 
that you will want to use.
- Anaconda serves as a package manager, with a long list of [3rd-party packages](https://docs.anaconda.com/anaconda/packages/pkg-docs/) that it knows about.
- We will be using python 3. Take care - managing different python environments is [notoriously problematic](https://xkcd.com/1987/)!
- Mathematica can be downloaded for Mac or Win10 (for free, site-wide license) from [software.caltech.edu](http://software.caltech.edu/)
- Assignment 20.2 is about Unix. Your Mac has unix under the hood, so you can do most or all of the assignment there. 
If you have Windows, either (a) use the physlab cluster, (b) install [Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/about), or 
(c) install linux in a virtual machine (eg, [VirtualBox](https://www.virtualbox.org/)). 
(DON'T bother with cygwin, and you probably don't want to bother with dual-booting). Google it!
- Stackoverflow is your friend!
- You will learn how to use the Ph2x [git repository](https://bayard.caltech.edu/ph2x/info), and use it for your Ph20-22 work.

---
## Setting up your laptop for Ph 20-22 work

- Here's some advice on setting up your laptop for scientific computing. If you have other preferences, please feel free to share with the class (or talk to us)!
- Mac (personal experience): This website a good place to start from: [Setup a New Mac for Astronomy])(http://www.astrobetter.com/wiki/Setup+a+New+Mac+for+Astronomy)
- Python: Install [Anaconda](https://conda.io/docs/install/full.html) and `conda update --all`
- There are many good python editors available. For example, "jupyter notebook" is great for displaying both code and plots, and "telling stories".
[Spyder](https://www.spyder-ide.org/) is good at organizing relatively long/complex code.
Lots of people like [pycharm](https://www.jetbrains.com/pycharm).
- Latex: Install MacTex for Latex and set up following [these guidelines](https://www.pydanny.com/setting-up-latex-on-mac-os-x.html). [Overleaf](https://www.overleaf.com/) is also a popular option, especially for collaborating on paper writing. If you choose to use overleaf, please make sure that you are equally comfortable with local latex editors (e.g. MacTex), and understand how to compile files (useful for writing papers).
- One way to start on latex is to play with a template. Here is a very basic example: [hw0_template.tex](http://pmaweb.caltech.edu/~physlab/hw0_template.tex). In order to see it, you'll need to compile it with your latex editor (after installing a few packages). There are plenty more templates online, and online editor like overleaf.
- Linux: to remotely log into a linux machine from Mac's terminal, you need to install XQuartz & X11. In addition, later in the term you might want to mount remote directories onto your laptop using, for example, sshfx, Sftp or rsync. This makes it easier to transfer files around.
- At some point, you might also want to Install Xcode (from Apple Store), edit your ".bash_profile" (to set up alias, change color), personalize you favorite text editors, and so on.
- If you have a Linux system laptop, you just need to install Anaconda. Most of the other tools should be already there :)
- Windows? The Ph 20/21/22 staff don't have much experience with it as a platform for scientific computing. Anaconda is available for Windows, and may provide a sufficiently workable computing environment. Mathematica for Windows presumably works fine. But you may find it better to just remotely log in to the ph20 lab's linux machines. Recent versions of Win10 include an ssh client, which may need to be [enabled](https://www.bleepingcomputer.com/news/microsoft/heres-how-to-enable-the-built-in-windows-10-openssh-client/). Else, you can download, install, and run [PuTTY](https://putty.org/), which should work fine. Please practice remote login and play with it as early as possible, and let us know if anything is unclear!


## What to hand in?
- Latex file and figures. You can refer to this [example problem](http://pmaweb.caltech.edu/~physlab/ExampleProblemSet.pdf) (adapted from Ge/Ay 117, but our class won't accept handwritten solutions). The point is to explain things clearly, and make meaningful figures.
- Your code, in a file like `name_hwX.py` or a Jupyter notebook `name_hwX.ipynb` .
- Put it all in your git repo, and alert the TA by email.
