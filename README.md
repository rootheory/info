# Ph 20, 21, 22
## Spring 2021 Organizational meeting, Monday Mar 29 at 3pm PT, [zoom link](https://caltech.zoom.us/j/84104808040) (you must be logged in to zoom with your caltech.edu credentials)

## Spring 2021 Recurring Sessions, To Be Determined soon! Dates to be updated, soon!

Physics 20, 21, 22 is a sequence of computational lab courses that covers a variety of tools for scientific computation including: python, Mathematica, Unix tools, numerical methods, and tools for data analysis.

- Each course is 6 units (0-6-0)
- Prerequisites: CS 1 or equivalent experience with computers 
- Offered first, second and third terms
- Ph 21 builds on Ph 20, and Ph 22 builds on Ph 21. So, in principle, the preceding course in the sequence is a prerequisite.
- However, this is not strictly enforced. If you feel sufficiently confident in your skills, you can take the courses independently, and even concurrently (but that's not recommended).
- All of the class material is on our [git repository](https://bayard.caltech.edu/ph2x/info).
- The [ground rules](https://bayard.caltech.edu/ph2x/info/-/blob/master/ground_rules.md) for the class is a long list, but please do read it all carefully.
- If you find any issues with this course site (git project) or any of the assignments, or anything else you want to bring to the attention of the course staff, 
you can use the "Issues" tracker over on the left, under "Repository". 
- At this time, we don't anticipate you needing anything on the Canvas site - it's all in the git repository.
- In the **spring** term, **seniors** need to finish all of their work one week earlier than the rest of the class!

----
## In the time of covid:
- In Spring 2021, the entire course will be conducted online with Zoom meetings. You must log on to zoom with your access.caltech.edu credentials.
See the [zoom info in learn.caltech.edu](https://learn.caltech.edu/tech-tools/zoom) .
- Tentatively: We will hold an Organizational Meeting (OM) for Ph 20,21,22 on Monday Jan 4 at 3pm PT, on zoom. 
- This OM is open to any Caltech undergrad who is interested in taking Ph 20, 21,22 this Fall, whether you have pre-registered or not. 
- However, only students who have registered can access the Canvas site, containing the zoom link to the OM. 
The zoom info is posted above. Email [Alan Weinstein](mailto:ajw@caltech.edu) and [Jacob Shen](mailto:xshen@caltech.edu) if you have problems / questions.
- When the campus opens up, the class will meet in the Physics Computation Lab, a computer lab in room #304 of the East Bridge building on the Caltech campus. The lab is managed by Chris Mach.

----
## Staff
- Prof: [Alan Weinstein](mailto:ajw@caltech.edu)
- Physics Computation Lab (WB 304) manager: [Chris Mach](mailto:cmach@caltech.edu)
- TA: [Jacob Shen](mailto:xshen@caltech.edu)
- TA: [Liam O'Brien](mailto:lobrien@caltech.edu)

---
## Description of Courses (Syllabus)
Physics 20, 21, 22 is a sequence of computational lab courses that covers a variety of tools for scientific computation including: python, Mathematica, Unix tools, numerical methods, and tools for data analysis .
- [Physics 20 (Spring 2021)](ph20.md) , [Canvas site](https://caltech.instructure.com/courses/2836)
- [Physics 21 (Spring 2021)](ph21.md) , [Canvas site](https://caltech.instructure.com/courses/2837)
- [Physics 22 (Spring 2021)](ph22.md) , [Canvas site](https://caltech.instructure.com/courses/2838)
- At this time, we don't anticipate you needing anything on the Canvas site - it's all in this git repository. 
On the other hand, there may be useful tools there; eg, the Chat room. If you find that Canvas has anything useful for us, let the prof and TAs know.


---
## General course info and advice - *required reading for all students*
- [Ground Rules](ground_rules.md) - including Collaboration Policy
- [Conda setup advice](computing_help/setup_conda.md)
- [git project setup advice](computing_help/setup_git_project.md)
- [style guide](computing_help/style_guide.md)

--- 

## Wellness and Inclusion
- It is very important to us that you maintain your mental wellness throughout the course. A few points are not worth losing sleep over. 
The prof and TAs are available to chat, and you can always attend office hours for a non-academic conversation if necessary. 
You can also visit the counseling center if you find you need help beyond the course staff. 
If you have a temporary health condition or permanent disability (either mental health or physical health related), 
you should contact disability services if you have not already. 
Additionally, if there is something we can do to make your experience better, please let us know.

- Diversity, inclusion, and belonging are all core values of this course and at Caltech. 
All participants in this course must be treated with respect by other members of the community in accordance with the honor code. 
If you feel unwelcome or unsafe in any way, no matter how minor, we encourage you to talk to the prof or TAs, or one of the Deans. 
We view these sorts of honor code violations as completely unacceptable, and we take them very seriously.

- If you need an extension FOR ANY REASON, send an e-mail to the prof and your TA.  


## Resources - we welcome suggestions for good resource links

- [Git Tutorial](https://hackernoon.com/understanding-git-fcffd87c15a3)
- [Linux tutorials.](http://physlab.blogspot.com/2004/09/linuxunix-tutorials.html#comments)
- [Python 3 Documentation](https://docs.python.org/3/)
- Python information (on wiki).
- [Compusalon - Michele Vallisneri (Python)](http://vallis.org/book/code.html)
- [Software Carpentry.](http://software-carpentry.org/)
- [Tutorial on using Makefiles.](http://www.pmaweb.caltech.edu/~physlab/make.html)
- [Numerical Recipes in C (the book).](http://apps.nrbook.com/c/index.html)
- [C++ Tutorial](http://www.cplusplus.com/doc/tutorial), [GCC Manual](https://gcc.gnu.org/onlinedocs/gcc-6.5.0/gcc/) and [C/C++ Standard Library reference](https://en.cppreference.com/w/)
- [IEEE 754 Floating Point Format](https://en.wikipedia.org/wiki/IEEE_754)
- [Floating point number comparison](https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/)
- [Maximum-Likelihood Data Analysis and Fitting](https://arxiv.org/pdf/1210.3781.pdf) (Mainly bootstrap analysis for Monte Carlo simulations)
